import {Component, Inject, OnInit, ViewChild} from '@angular/core';
import {Ficha, Motivo} from "../model/ficha.model";
import {SistemaMedicoService} from "../sistema-medico.service";
import {DatePipe} from "@angular/common";
import {stringify} from "@angular/compiler/src/util";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Paciente} from "../model/paciente.model";
import {FormControl, Validators} from "@angular/forms";
import {MatTable} from "@angular/material/table";
import {NavigationExtras, Router} from "@angular/router";
import {Medico} from "../model/medico.model";

@Component({
  selector: 'app-gestion-fichas',
  templateUrl: './gestion-fichas.component.html',
  styleUrls: ['./gestion-fichas.component.css']
})
export class GestionFichasComponent implements OnInit {

  public mensajeExito: string = '';
  public mensajeError: string = '';
  public id_paciente: number = 0;
  public nombre_paciente: string = '';
  public apellido_paciente: string = '';
  public cedula_paciente: number = 0;
  public motivos: Motivo[] = [];
  public texto_motivo: string = "";
  public diagnostico: string = "";
  public tratamiento: string = "";
  public date: string = "";
  public fecha = new Date();
  public paciente: Paciente = {id_paciente:0,email_paciente:"",nombre_paciente:"",cedula_paciente:0,telefono_paciente:"",apellido_paciente:"",fecha_nacimiento_paciente:""}
  public medico: Medico = {id_medico:0,email_medico:"",nombre_medico:"",cedula_medico:0,telefono_medico:"",apellido_medico:"",password:"",especialidad:"",usuario:"",fecha_nacimiento:""}
  public motivoControl=new FormControl('',Validators.required)
  public diagnosticoControl=new FormControl('',Validators.required)
  public tratamientoControl = new FormControl('',Validators.required)
  public motivoAux: Motivo = {id_motivo:0, texto_motivo: "", diagnostico:"", tratamiento:""};
  public ficha: Ficha= {id_ficha:0,fecha:new Date(),medico:this.medico,paciente:this.paciente,detallesConsulta:this.motivos};
  public nombreApMedico: string = "";
  public especialidad: string = "";
  public usuario:any;
  // @ts-ignore
  public dataSource = this.motivos;
  public columns = ['texto_motivo','diagnostico','tratamiento']

  @ViewChild(MatTable) table: MatTable<Motivo>;

  constructor(private servicio: SistemaMedicoService,
              public dialog: MatDialog,
              private router:Router) {
    // @ts-ignore
    if (this.router.getCurrentNavigation().extras.state) {
      // @ts-ignore
      let user = this.router.getCurrentNavigation().extras.state.user;
      this.usuario = user;
    }
  }

  ngOnInit(): void {
    this.servicio.getBuscarMedicoUsuarioPassword(this.usuario.usuario, this.usuario.password)
      .subscribe((medico) => {
          this.medico = medico;
          this.nombreApMedico = this.medico.nombre_medico + " " + this.medico.apellido_medico;
          this.especialidad = this.medico.especialidad;
          console.log(this.nombreApMedico);
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });


  }

  agregarMotivo(texto_motivo:string, diagnostico:string, tratamiento:string){
    this.motivoAux.texto_motivo = texto_motivo;
    this.motivoAux.diagnostico = diagnostico;
    this.motivoAux.tratamiento = tratamiento;
    this.motivos.push({id_motivo:0,texto_motivo:texto_motivo,diagnostico:diagnostico,tratamiento:tratamiento});
    this.table.renderRows();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog, {
      width: '800px',
      data: {nombre: this.nombre_paciente, apellido: this.apellido_paciente}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.paciente = result;
      this.nombre_paciente = this.paciente.nombre_paciente;
      this.apellido_paciente = this.paciente.apellido_paciente;
      this.cedula_paciente = this.paciente.cedula_paciente;
    });

  }

  crearFicha(){
    this.mensajeExito = '';
    this.mensajeError = '';
    this.ficha.paciente = this.paciente;
    this.ficha.medico = this.medico;
    this.ficha.detallesConsulta = this.motivos;
   /* var dd = String(this.fecha.getDate()).padStart(2, '0');
    var mm = String(this.fecha.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = this.fecha.getFullYear();

    this.date = dd + '/' + mm + '/' + yyyy; */

    // @ts-ignore
    this.servicio.crearFicha(this.ficha)
      .subscribe((fichaCreada: Ficha) => {
          this.mensajeExito = `La ficha de ${this.nombre_paciente} ${this.apellido_paciente} fue creada exitosamente`;
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
  }
}




@Component({
  selector: 'dialog-overview-example-dialog',
  templateUrl: 'dialog-overview-example-dialog.html',
  styleUrls: ['./gestion-fichas.component.css']
})
export class DialogOverviewExampleDialog {

  public mensajeExito: string = '';
  public mensajeError: string = '';

  public columns = ['nombre','apellido','cedula']

  // @ts-ignore
  @ViewChild(MatTable) table: MatTable<Paciente>;
  public pacientes: Paciente[] = [];


  // @ts-ignore
  public dataSource = this.pacientes;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog>,
    @Inject(MAT_DIALOG_DATA) public data: Paciente,
    private servicio: SistemaMedicoService) {

  }


  onNoClick(): void {
    this.dialogRef.close();
  }

  buscarPaciente(nombre: string, apellido: string){
    this.mensajeError = '';
    this.mensajeExito = '';
    this.servicio.getBuscarPacientesPorNombreApellido(nombre, apellido)
      .subscribe((pacientesEncontrados: Paciente[]) => {
          this.pacientes = pacientesEncontrados;
          this.dataSource = this.pacientes;
          this.table.renderRows();
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
  }

  seleccionarPaciente(nombre:string, apellido:string, cedula:number){
    this.data.nombre_paciente = nombre;
    this.data.apellido_paciente = apellido;
    this.data.cedula_paciente = cedula;
  }

}





