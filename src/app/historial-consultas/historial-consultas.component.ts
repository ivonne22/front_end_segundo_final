import {Component, OnInit, ViewChild} from '@angular/core';
import {SistemaMedicoService} from "../sistema-medico.service";
import {Motivo} from "../model/ficha.model";
import {Ficha} from "../model/ficha.model";
import {MatSort} from "@angular/material/sort";
import {animate, state, style, transition, trigger} from "@angular/animations";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-historial-consultas',
  templateUrl: './historial-consultas.component.html',
  styleUrls: ['./historial-consultas.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})

export class HistorialConsultasComponent implements OnInit {

  public loading = true;
  public mensajeError: string = "";
  public mensajeExito: string = "";
  public consultas: Consulta[] = [];
  public fichas: Ficha_historial[]=[];
  public ficha: Ficha_historial = {id_ficha:0, Paciente:"", Medico:"", especialidadMedico:"", fechaFicha:new Date(), motivos:""};
  public auxFichas: Ficha_historial[]=[];
  public moti: Motivo={id_motivo:0,texto_motivo:"",tratamiento:"",diagnostico:""}
  public motivos: Motivo[] = [];

  public dataSource= new MatTableDataSource(this.fichas);
  public columnsToDisplay = ['id_ficha','Paciente','Medico','especialidadMedico','fechaFicha']
  // @ts-ignore
  expandedElement: Ficha_historial | null;

  // @ts-ignore
  @ViewChild(MatSort, {static: true}) sort: MatSort;


  constructor(private servicio: SistemaMedicoService) {

  }

  ngOnInit(): void {

    this.servicio.getFichas()
      .subscribe((fichas) => {
          for(let fich of fichas){
            this.motivos = [];
            this.servicio.getMotivos(fich.id_ficha)
              .subscribe(value => {
                fich.motivos = value;
              });
          }
          this.auxFichas = fichas;
          for(let i of this.consultas){
            let varAux = "";
            for(let j of i.motivos){
              varAux = varAux + "Motivo: " + j.texto_motivo.toString() + "\tDiagnostico: " + j.diagnostico.toString() + "\tTratamiento " + j.tratamiento.toString() + " " + " \n";
            }
            this.fichas.push({id_ficha:i.id_ficha, Paciente:i.Paciente, Medico:i.Medico, especialidadMedico:i.especialidadMedico, fechaFicha:i.fechaFicha, motivos:varAux})
            this.loading = false;
          }

        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });

   /* this.servicio.getFichas()
      .subscribe(value => {
        this.consultas = value;
        this.loading=false;
      }); */
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  /* filtro(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ordenar(){
    this.dataSource.sort = this.sort;
  } */

}


export interface Consulta{
  id_ficha:number;
  Paciente:string;
  Medico:string;
  especialidadMedico:string;
  fechaFicha:Date;
  motivos:Motivo[];
}

export interface Ficha_historial{
  id_ficha:number;
  Paciente:string;
  Medico:string;
  especialidadMedico:string;
  fechaFicha:Date;
  motivos:string;
}
