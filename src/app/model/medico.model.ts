export interface Medico {
  id_medico: number;
  nombre_medico: string;
  apellido_medico: string;
  cedula_medico: number;
  email_medico: string;
  telefono_medico: string;
  fecha_nacimiento: string;
  especialidad: string;
  usuario: string;
  password: string;
}
