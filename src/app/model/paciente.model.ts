export interface Paciente {
  id_paciente: number;
  nombre_paciente: string;
  apellido_paciente: string;
  cedula_paciente: number;
  email_paciente: string;
  telefono_paciente: string;
  fecha_nacimiento_paciente: string;
}
