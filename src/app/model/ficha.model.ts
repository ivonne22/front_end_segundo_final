import {Paciente} from "./paciente.model";
import {Medico} from "./medico.model";

export interface Ficha {
  id_ficha: number;
  fecha: Date;
  paciente: Paciente;
  medico: Medico;
  detallesConsulta: Motivo[];
}

export interface Motivo{
  id_motivo: number;
  texto_motivo: string;
  diagnostico: string;
  tratamiento: string;
}
