import { TestBed } from '@angular/core/testing';

import { SistemaMedicoService } from './sistema-medico.service';

describe('SistemaMedicoService', () => {
  let service: SistemaMedicoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SistemaMedicoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
