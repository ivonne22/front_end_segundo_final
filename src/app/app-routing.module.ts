import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {GestionFichasComponent} from "./gestion-fichas/gestion-fichas.component";
import {HistorialConsultasComponent} from "./historial-consultas/historial-consultas.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'ficha',
    component: GestionFichasComponent
  },
  {
    path: 'historial',
    component: HistorialConsultasComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
