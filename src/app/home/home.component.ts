import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {Medico} from "../model/medico.model";
import {DialogOverviewExampleDialog} from "../gestion-fichas/gestion-fichas.component";
import {NavigationExtras} from "@angular/router";
import {Router} from "@angular/router";
import {SistemaMedicoService} from "../sistema-medico.service";
import {Paciente} from "../model/paciente.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public medico: Medico = {id_medico:0,email_medico:"",nombre_medico:"",cedula_medico:0,telefono_medico:"",apellido_medico:"",password:"",especialidad:"",usuario:"",fecha_nacimiento:""}
  public mensajeExito: string = '';
  public mensajeError: string = '';

  constructor(public dialog: MatDialog,
              private router: Router,
              private servicio: SistemaMedicoService) { }

  ngOnInit(): void {

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialog2, {
      width: '300px',
      data: {usuario: this.medico.usuario, password: this.medico.usuario}
    });

    dialogRef.afterClosed().subscribe(result => {
      let user={usuario:result.usuario,password:result.password}
      let navigationExtras: NavigationExtras = {
        state: {
          user: user
        }
      };
      this.router.navigate(['/ficha'],navigationExtras);

    });

  }
}




@Component({
  selector: 'dialog-overview-example-dialog2',
  templateUrl: 'dialog-overview-example-dialog2.html',
  styleUrls: ['./home.component.css']
})
export class DialogOverviewExampleDialog2 {

  public medicos: Medico[] = [];
  public mensajeExito: string = '';
  public mensajeError: string = '';


  public dataSource = this.medicos;

  constructor(
    public dialogRef: MatDialogRef<DialogOverviewExampleDialog2>,
    @Inject(MAT_DIALOG_DATA) public data: Medico) {

  }


  onNoClick(): void {
    this.dialogRef.close();
  }

}

