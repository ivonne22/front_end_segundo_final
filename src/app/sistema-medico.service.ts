import { Injectable } from '@angular/core';
import {Motivo} from "./model/ficha.model";
import {Ficha} from "./model/ficha.model";
import {Paciente} from "./model/paciente.model";
import {HttpClient} from "@angular/common/http";
import {environment} from "../environments/environment";
import {Medico} from "./model/medico.model";

@Injectable({
  providedIn: 'root'
})
export class SistemaMedicoService {
  private URL_BASE = environment.API_URL;

  constructor(private http: HttpClient) { }
  crearFicha(ficha:Ficha) {
    return this.http.post<Ficha>(`${this.URL_BASE}/ficha/crear`,{ficha});
  }
  getPacienteCedula(cedula_paciente: number){
    return this.http.get<Paciente>(`${this.URL_BASE}/paciente/${cedula_paciente}`);
  }

  getFichas(){
    return this.http.get<any[]>(`${this.URL_BASE}/ficha/listar`);
  }

  getBuscarMedicoUsuarioPassword(usuario:string, password:string) {
    return this.http.get<Medico>(`${this.URL_BASE}/medico/${usuario}/${password}`);
  }

  getBuscarPacientesPorNombreApellido(nombre: string, apellido: string) {
    return this.http.get<Paciente[]>(`${this.URL_BASE}/paciente/${nombre}/${apellido}`);
  }

  getMotivos(id_ficha:number){
    return this.http.get<Motivo[]>(`${this.URL_BASE}/motivo/${id_ficha}`);
  }
}
